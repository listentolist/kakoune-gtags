provide-module gtags %§

require-module connect

declare-option -hidden completions gtags_completions
declare-option -docstring "minimum characters before triggering autocomplete" \
    int gtags_min_chars 3

define-command gtags-generate -params .. -docstring %{
    gtags-generate [<arguments>]: generate gtags files
    All the <arguments> are forwarded to the `gtags` command.
  } %{
  echo -markup "{Information}gtags-generate started"
  $ sh -c %{
    eval gtags "$@"
    :send echo -markup "{Information}gtags-generate finished"
  } -- %arg{@}
}

define-command gtags-update -docstring "Update gtags files" %{
  echo -markup "{Information}gtags-update started"
  $ sh -c %{
    global -u
    :send echo -markup "{Information}gtags-update finished"
  }
}

define-command gtags -params 1.. -docstring %{
    gtags [<arguments>]: global wrapper
    All the <arguments> are forwarded to the `global` command.
    Open a menu with the matches.
  } %{
  try %{
    evaluate-commands %sh{
      echo menu -auto-single $(eval global --result=grep "$@" | awk -F ':' '{
        printf "\047%s: ", $1;
        for (i = 3; i <= NF; i++) printf $i;
        printf "\047 \047edit %s %s\047 ", $1, $2;
      }')
    }
  } catch %{
    fail gtags: tag not found
  }
}

define-command gtags-scratch -params 1.. -docstring %{
    gtags-scratch [<arguments>]: global wrapper
    All the <arguments> are forwarded to the `global` command.
    Open a scratch buffer with the matches.
  } -shell-script-candidates %{
    global -t -d ".*" | awk '{ print $1}'
  } %{
  evaluate-commands -try-client %opt{toolsclient} %{
    $ sh -c %{
      mkfifo gtags.fifo
      :send edit! -fifo gtags.fifo '*gtags*'
      :send set-option buffer filetype grep
      :send set-option buffer grep_current_line 1
      eval global --result=grep "$@" > gtags.fifo
      rm gtags.fifo
    } -- %arg{@}
  }
}

# the following commands are taken from grep.kak

define-command gtags-next-match -docstring %{
    gtags-next-match: jump to next match in *gtags* buffer
  } %{
  evaluate-commands -try-client %opt{jumpclient} %{
    buffer '*gtags*'
    execute-keys "ge %opt{grep_current_line}g<a-l> /^[^:]+:\d+:<ret>"
    grep-jump
  }
  try %{
    evaluate-commands -client %opt{toolsclient} %{
      buffer '*gtags*'
      execute-keys gg %opt{grep_current_line}g
    }
  }
}

define-command gtags-previous-match -docstring %{
    gtags-previous-match: jump to previous match in *gtags* buffer
  } %{
  evaluate-commands -try-client %opt{jumpclient} %{
    buffer '*gtags*'
    execute-keys "ge %opt{grep_current_line}g<a-h> <a-/>^[^:]+:\d+:<ret>"
    grep-jump
  }
  try %{
    evaluate-commands -client %opt{toolsclient} %{
      buffer '*gtags*'
      execute-keys gg %opt{grep_current_line}g
    }
  }
}

define-command gtags-definition -params 1 -docstring %{
    gtags-definition [<symbol>]: go to definition of <symbol>
  } -shell-script-candidates %{
    global -t -d ".*" | awk '{ print $1}'
  } %{
  gtags -d %arg{@}
}

define-command gtags-references -params 1 -docstring %{
    gtags-references [<symbol>]: open buffer with references to <symbol>
  } -shell-script-candidates %{
    global -t -r ".*" | awk '{ print $1}'
  } %{
  gtags-scratch -r %arg{@}
}

define-command gtags-context -params 0.. -docstring %{
    gtags-context [<symbol>]: go to definition or references
    global decides by context which tag type to choose
  } %{
  gtags-scratch "--from-here=%val{cursor_line}:%val{buffile}" %arg{@}
}

# completion

define-command -hidden gtags-complete %{
  $ sh -c %{
    header="$1.$2@$3"
    compl=$(
      global -c "$4" | awk '{ printf " %1$s||%1$s", $1 }'
    )
    :send set-option buffer="$(:it)" gtags_completions "$header $compl"
  } -- %val{cursor_line} %val{cursor_column} %val{timestamp} %val{selection}
}

# autocomplete commands are taken directly from ctags.kak

define-command gtags-enable-autocomplete -docstring "Enable automatic gtags completion" %{
  set-option -add window completers "option=gtags_completions"
  hook window -group gtags-autocomplete InsertIdle .* %{
    try %{
      evaluate-commands -draft %{
        execute-keys "<space>b_<a-k>.{%opt{gtags_min_chars},}<ret>"
        gtags-complete
      }
    }
  }
}

define-command gtags-disable-autocomplete -docstring "Disable automatic gtags completion" %{
  set-option -remove window completers 'option=gtags_completions'
  remove-hooks window gtags-autocomplete
}

§
