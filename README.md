# kakoune-gtags

This plugin provides support for [GNU
GLOBAL](https://www.gnu.org/software/global/) in the
[Kakoune](https://kakoune.org/) text editor.

The commands are very similiar to the built-in
[ctags.kak](https://github.com/mawww/kakoune/blob/master/rc/tools/ctags.kak).
Many parts are actually just copied from it and adjusted for *GNU GLOBAL*.

## Installation

Put the file `gtags.kak` into the autoload directory
and run `require-module gtags`. You need to have the
[connect.kak](https://github.com/alexherbo2/connect.kak) plugin installed.

## Usage

- `gtags-generate [<arguments>]`: generate gtags files\
  All the arguments are forwarded to the `gtags` command.
- `gtags-update`: Update gtags files
- `gtags [<arguments>]`: global wrapper\
  All the arguments are forwarded to the `global` command.\
  Open a menu with the matches.
- `gtags-scratch [<arguments>]`: global wrapper\
  All the arguments are forwarded to the `global` command.\
  Open a scratch buffer with the matches.
- `gtags-next-match`: jump to next match in `*gtags*` buffer
- `gtags-previous-match`: jump to previous match in `*gtags*` buffer
- `gtags-definition [<symbol>]`: go to definition of symbol
- `gtags-references [<symbol>]`: open buffer with references to symbol
- `gtags-context [<symbol>]`: go to definition or references\
   global decides by context which tag type to choose
- `gtags-enable-autocomplete`: Enable automatic gtags completion
- `gtags-disable-autocomplete` Disable automatic gtags completion

## Configuration

- `int gtags_min_chars`, default `3`\
  minimum characters before triggering autocomplete

### Suggested Mappings

```
map global goto d "<space><a-i>w: gtags-definition <c-r>.<ret>"
map global goto r "<space><a-i>w: gtags-references <c-r>.<ret>"
map global goto o "<space><a-i>w: gtags-context <c-r>.<ret>"
```
